// ** Icon imports
import UserOutline from 'mdi-material-ui/AccountGroupOutline';
import DashboardOutline from 'mdi-material-ui/ViewDashboardOutline';

// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types';

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: 'Dashboard',
      icon: DashboardOutline,
      path: '/',
    },
    {
      title: 'Users',
      icon: UserOutline,
      path: '/users',
    },
  ];
};

export default navigation;
