// ** Icon imports
import HomeOutline from 'mdi-material-ui/HomeOutline';
import EmailOutline from 'mdi-material-ui/EmailOutline';

// ** Type import
import { HorizontalNavItemsType } from 'src/@core/layouts/types';

const navigation = (): HorizontalNavItemsType => [
  {
    title: 'Home',
    icon: HomeOutline,
    path: '/',
  },
  {
    title: 'Second Page',
    icon: EmailOutline,
    path: '/second-page',
  },
];

export default navigation;
