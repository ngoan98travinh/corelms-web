export enum UserRole {
  Administrator = 'Administrator',
  Manager = 'Manager',
  Teacher = 'Teacher',
  Student = 'Student',
  Supervisor = 'Supervisor',
}

export enum UserStatus {
  Active = 'Active',
  Pending = 'Pending',
  Inactive = 'Inactive',
}

export interface User {
  _id: string;
  createdAt: Date;
  createdBy: string;
  updatedAt: Date;
  updatedBy: string;
  username: string;
  tel: string;
  email: string;
  displayName: string;
  title: string;
  avatar: string;
  roles: UserRole[];
  status: UserStatus;
}
