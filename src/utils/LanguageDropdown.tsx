// ** React Imports
import { Fragment, SyntheticEvent, useState } from 'react';

// ** MUI Imports
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import IconButton from '@mui/material/IconButton';

// ** Icons Imports
import Translate from 'mdi-material-ui/Translate';

// ** Third Party Import
import { useTranslation } from 'react-i18next';

// ** Type Import
import { Settings } from 'src/@core/context/settingsContext';

interface Props {
  settings: Settings;
  saveSettings: (values: Settings) => void;
}

const LanguageDropdown = ({ settings, saveSettings }: Props) => {
  // ** State
  const [anchorEl, setAnchorEl] = useState<any>(null);

  // ** Hook
  const { i18n } = useTranslation();

  // ** Vars
  const { layout, direction } = settings;

  const handleLangDropdownOpen = (event: SyntheticEvent) => {
    setAnchorEl(event.currentTarget);
  };

  const handleLangDropdownClose = () => {
    setAnchorEl(null);
  };

  const handleLangItemClick = (lang: 'en' | 'vi' | 'ko') => {
    i18n.changeLanguage(lang);
    handleLangDropdownClose();
  };

  return (
    <Fragment>
      <IconButton
        color='inherit'
        aria-haspopup='true'
        aria-controls='customized-menu'
        onClick={handleLangDropdownOpen}
        sx={layout === 'vertical' ? { mr: 0.75 } : { mx: 0.75 }}
      >
        <Translate />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleLangDropdownClose}
        sx={{ '& .MuiMenu-paper': { mt: 4, minWidth: 130 } }}
        anchorOrigin={{ vertical: 'bottom', horizontal: direction === 'ltr' ? 'right' : 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: direction === 'ltr' ? 'right' : 'left' }}
      >
        <MenuItem
          sx={{ py: 2 }}
          selected={i18n.language === 'en'}
          onClick={() => {
            handleLangItemClick('en');
            saveSettings({ ...settings, direction: 'ltr' });
          }}
        >
          English
        </MenuItem>
        <MenuItem
          sx={{ py: 2 }}
          selected={i18n.language === 'vi'}
          onClick={() => {
            handleLangItemClick('vi');
            saveSettings({ ...settings, direction: 'ltr' });
          }}
        >
          Vietnamese
        </MenuItem>
        <MenuItem
          sx={{ py: 2 }}
          selected={i18n.language === 'ko'}
          onClick={() => {
            handleLangItemClick('ko');
            saveSettings({ ...settings, direction: 'ltr' });
          }}
        >
          Korean
        </MenuItem>
      </Menu>
    </Fragment>
  );
};

export default LanguageDropdown;
