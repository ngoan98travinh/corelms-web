// ** Next Import
import Link from 'next/link';

// ** MUI Imports
import Grid from '@mui/material/Grid';
import Alert from '@mui/material/Alert';

// ** Demo Components Imports
import UserViewLeft from 'src/views/users/view/UserViewLeft';
import UserViewRight from 'src/views/users/view/UserViewRight';
import { useRouter } from 'next/router';
import { useQuery } from '@apollo/client';
import { GetUserDetailParams, GetUserDetailRes, GET_USER_DETAIL } from 'src/queries/user-detail.queries';

const UserDetailPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data, error } = useQuery<GetUserDetailRes, GetUserDetailParams>(GET_USER_DETAIL, {
    variables: {
      id: id as string,
    },
  });

  if (data) {
    return (
      <Grid container spacing={6}>
        <Grid item xs={12} md={5} lg={4}>
          <UserViewLeft data={data.user} />
        </Grid>
        <Grid item xs={12} md={7} lg={8}>
          <UserViewRight invoiceData={[]} />
        </Grid>
      </Grid>
    );
  } else if (error) {
    return (
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <Alert severity='error'>
            User with the id: {id} does not exist. Please check the list of users:{' '}
            <Link href='/apps/user/list'>User List</Link>
          </Alert>
        </Grid>
      </Grid>
    );
  } else {
    return null;
  }
};

export default UserDetailPage;
