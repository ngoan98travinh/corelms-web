import { User } from 'src/@interfaces/user.interface';
import { CreateUserInput } from './create-user-input.type';
import { LoginInput } from './login-input.type';

export type ErrCallbackType = (err: { [key: string]: string }) => void;

export type AuthValuesType = {
  loading: boolean;
  setLoading: (value: boolean) => void;
  logout: () => void;
  isInitialized: boolean;
  user: User | null;
  setUser: (value: User | null) => void;
  setIsInitialized: (value: boolean) => void;
  login: (params: LoginInput, errorCallback?: ErrCallbackType) => void;
  register: (params: CreateUserInput, errorCallback?: ErrCallbackType) => void;
};
