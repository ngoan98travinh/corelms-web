import { UserRole } from 'src/@interfaces/user.interface';

export type CreateUserInput = {
  username: string;
  password: string;
  displayName: string;
  title: string;
  tel: string;
  email: string;
  avatar: string;
  roles: UserRole[];
};
