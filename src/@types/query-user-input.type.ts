import { UserRole } from 'src/@interfaces/user.interface';

export type QueryUserInput = {
  limit: number;
  offset: number;
  roles?: UserRole;
};
