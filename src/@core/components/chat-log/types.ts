export type ChatLogType = {
  hidden: boolean;
  data: {
    chat: any;
    contact: any;
    userContact: any;
  };
};

export type FeedbackType = any;

export type MessageType = {
  time: string | Date;
  message: string;
  senderId: number;
  feedback: any;
};

export type ChatType = {
  msg: string;
  time: string | Date;
  feedback: any;
};

export type FormattedChatsType = {
  senderId: number;
  messages: ChatType[];
};

export type MessageGroupType = {
  senderId: number;
  messages: ChatType[];
};
