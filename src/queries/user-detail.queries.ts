import { gql } from '@apollo/client';
import { User } from 'src/@interfaces/user.interface';

export const GET_USER_DETAIL = gql`
  query getUserDetail($id: String!) {
    user(id: $id) {
      _id
      username
      createdAt
      tel
      email
      roles
      status
      avatar
      displayName
      title
    }
  }
`;

export type GetUserDetailRes = {
  user: User;
};

export type GetUserDetailParams = {
  id: string;
};
