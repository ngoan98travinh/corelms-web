import { gql } from '@apollo/client';
import { User } from 'src/@interfaces/user.interface';
import { QueryUserInput } from 'src/@types/query-user-input.type';

export const GET_USERS = gql`
  query getUsers($input: QueryUserInput!) {
    users(queryUserInput: $input) {
      _id
      username
      createdAt
      tel
      email
      roles
      status
      avatar
      displayName
    }
  }
`;

export type GetUsersRes = {
  users: User[];
};

export type GetUsersParams = {
  input: QueryUserInput;
};
