import { makeVar } from '@apollo/client';
import axios from 'axios';
import { Token } from 'src/@interfaces/token.interface';

export const tokenVar = makeVar<Token>({
  accessToken: '',
  refreshToken: '',
});

export async function getToken(): Promise<Token> {
  if (!tokenVar().accessToken && !tokenVar().refreshToken) {
    return axios.get<Token>('/api/jwt').then(res => {
      if (res.data.refreshToken) {
        tokenVar(res.data);
      } else {
        tokenVar({
          accessToken: 'none',
          refreshToken: 'none',
        });
      }

      return res.data;
    });
  }
  await new Promise(resolve => {
    let tried = 0;
    const timer = setInterval(() => {
      tried += 1;
      if (tokenVar().accessToken || tried > 10) {
        clearInterval(timer);
        resolve(0);
      }
    }, 1000);
  });

  return tokenVar();
}

export function postToken(token: Token) {
  const { accessToken, refreshToken } = token;

  return axios
    .post('/api/jwt', null, {
      headers: {
        'access-token': accessToken,
        'refresh-token': refreshToken,
      },
    })
    .then(res => {
      if (res.status === 201) {
        tokenVar(token);
      }
    });
}

export function clearToken() {
  return axios.delete('/api/jwt').then(res => {
    if (res.status === 200) {
      tokenVar({ accessToken: '', refreshToken: '' });
    }
  });
}
