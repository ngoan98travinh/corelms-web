import { gql } from '@apollo/client';
import { Token } from 'src/@interfaces/token.interface';
import { User } from 'src/@interfaces/user.interface';
import { CreateUserInput } from 'src/@types/create-user-input.type';
import { LoginInput } from 'src/@types/login-input.type';

export const GET_CURRENT_USER = gql`
  query getCurrentUser {
    currentUser {
      _id
      username
      displayName
      avatar
      roles
    }
  }
`;

export type GetCurrentUserRes = {
  currentUser: User;
};

export const USER_LOGIN = gql`
  mutation login($input: LoginInput!) {
    token: login(loginInput: $input) {
      accessToken
      refreshToken
    }
  }
`;

export type UserLoginParams = {
  input: LoginInput;
};

export type UserLoginRes = {
  token: Token;
};

export const CREATE_USER = gql`
  mutation createUser($input: CreateUserInput!) {
    createUser(createUserInput: $input) {
      _id
    }
  }
`;

export type CreateUserRes = {
  createUser: User;
};

export type CreateUserParams = {
  input: CreateUserInput;
};

export const REFRESH_TOKEN = gql`
  mutation refreshToken($userId: String!, $token: String!) {
    token: refresh(userId: $userId, refreshToken: $token) {
      accessToken
      refreshToken
    }
  }
`;

export type RefreshTokenRes = {
  token: Token;
};

export type RefreshTokenParams = {
  userId: string;
  token: string;
};
