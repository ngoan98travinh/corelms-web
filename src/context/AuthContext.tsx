// ** React Imports
import { createContext, useEffect, useState, ReactNode, useCallback } from 'react';

// ** Next Import
import { useRouter } from 'next/router';

// ** Types
import { useApolloClient, useMutation, useQuery, useReactiveVar } from '@apollo/client';
import {
  CreateUserParams,
  CreateUserRes,
  CREATE_USER,
  GetCurrentUserRes,
  GET_CURRENT_USER,
  RefreshTokenParams,
  RefreshTokenRes,
  REFRESH_TOKEN,
  UserLoginParams,
  UserLoginRes,
  USER_LOGIN,
} from './queries';
import { User } from 'src/@interfaces/user.interface';
import { AuthValuesType, ErrCallbackType } from 'src/@types/auth-context.type';
import { LoginInput } from 'src/@types/login-input.type';
import { clearToken, postToken, tokenVar } from 'src/configs/token';
import { CreateUserInput } from 'src/@types/create-user-input.type';

// ** Defaults
const defaultProvider: AuthValuesType = {
  user: null,
  loading: true,
  setUser: () => null,
  setLoading: () => Boolean,
  isInitialized: false,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  setIsInitialized: () => Boolean,
  register: () => Promise.resolve(),
};

const AuthContext = createContext(defaultProvider);

type Props = {
  children: ReactNode;
};

const AuthProvider = ({ children }: Props) => {
  // ** States
  const [user, setUser] = useState<User | null>(defaultProvider.user);
  const [loading, setLoading] = useState<boolean>(defaultProvider.loading);
  const [isInitialized, setIsInitialized] = useState<boolean>(defaultProvider.isInitialized);

  // ** Hooks
  const router = useRouter();
  const apolloClient = useApolloClient();
  const tokenValues = useReactiveVar(tokenVar);
  const {
    data: currentUserData,
    loading: currentUserLoading,
    refetch: refetchCurrentUser,
  } = useQuery<GetCurrentUserRes>(GET_CURRENT_USER);
  const [login] = useMutation<UserLoginRes, UserLoginParams>(USER_LOGIN);
  const [register] = useMutation<CreateUserRes, CreateUserParams>(CREATE_USER);
  const [refresh] = useMutation<RefreshTokenRes, RefreshTokenParams>(REFRESH_TOKEN);

  useEffect(() => {
    if (currentUserData?.currentUser) {
      setUser(currentUserData?.currentUser);
      window.localStorage.setItem('userData', JSON.stringify(currentUserData?.currentUser));
    }
  }, [currentUserData]);

  const handleLogout = useCallback(async () => {
    setUser(null);
    setIsInitialized(false);
    window.localStorage.removeItem('userData');
    await clearToken();
    await apolloClient.resetStore();
    router.push('/login');
  }, [apolloClient, router]);

  useEffect(() => {
    if (!tokenValues.accessToken && !!tokenValues.refreshToken && user) {
      refresh({
        variables: {
          userId: user?._id,
          token: tokenValues.refreshToken,
        },
      }).then(({ data, errors }) => {
        if (errors?.some(err => err.extensions['code'] === 'UNAUTHENTICATED')) {
          tokenVar({ accessToken: 'none', refreshToken: 'none' });
          handleLogout();
        } else if (data?.token) {
          postToken(data.token);
        }
      });
    }
  }, [tokenValues, user, handleLogout, refresh]);

  useEffect(() => {
    const initAuth = async (): Promise<void> => {
      setIsInitialized(true);
      const storedUser = window.localStorage.getItem('userData');
      if (storedUser && !user) {
        setUser(JSON.parse(storedUser));
      }
      setLoading(false);
    };
    initAuth();
  }, [user]);

  const handleLogin = (params: LoginInput, errorCallback?: ErrCallbackType) => {
    login({
      variables: { input: params },
    })
      .then(res => {
        if (res.data) {
          postToken(res.data.token).then(() => {
            refetchCurrentUser().then(() => {
              const returnUrl = router.query.returnUrl;
              const redirectURL = returnUrl && returnUrl !== '/' ? returnUrl : '/';
              router.replace(redirectURL as string);
            });
          });
        }
      })
      .catch(err => {
        if (errorCallback) errorCallback(err);
      });
  };

  const handleRegister = (params: CreateUserInput, errorCallback?: ErrCallbackType) => {
    register({
      variables: {
        input: params,
      },
    })
      .then(res => {
        if (res.data) {
          handleLogin({ username: params.username, password: params.password });
        }
      })
      .catch((err: { [key: string]: string }) => (errorCallback ? errorCallback(err) : null));
  };

  const values = {
    user,
    loading: loading || currentUserLoading,
    setUser,
    setLoading,
    isInitialized,
    setIsInitialized,
    login: handleLogin,
    logout: handleLogout,
    register: handleRegister,
  };

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};

export { AuthContext, AuthProvider };
