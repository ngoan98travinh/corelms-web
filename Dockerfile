FROM node:16 as builder

WORKDIR /home/app

EXPOSE 3000

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM node:16-alpine

WORKDIR /home/app

EXPOSE 3000

COPY package*.json ./

RUN npm install --omit=dev --ignore-scripts

COPY --from=builder /home/app/.next /home/app/.next
COPY --from=builder /home/app/public /home/app/public

CMD [ "npm", "start" ]
